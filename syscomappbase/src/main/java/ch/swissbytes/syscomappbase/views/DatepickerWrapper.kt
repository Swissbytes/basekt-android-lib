package ch.swissbytes.syscomappbase.views

import android.app.DatePickerDialog
import android.content.Context
import android.widget.DatePicker
import androidx.annotation.StyleRes
import ch.swissbytes.syscomappbase.R
import ch.swissbytes.syscomappbase.listeners.GenericListener
import java.util.*

class DatePickerWrapper(
    private val context: Context,
    @StyleRes val style: Int
) {

    companion object {
        @JvmStatic fun newInstance(
            context: Context,
            @StyleRes style: Int = R.style.DatePicker
        ) = DatePickerWrapper(context, style)
    }

    var maxDate: Long? = null
    var minDate: Long? = null
    var listener: GenericListener<Date>? = null
    var instanceDialog: DatePickerDialog? = null

    fun show() {
        if (instanceDialog == null) {

            val c = Calendar.getInstance()
            val mYear = c.get(Calendar.YEAR)
            val mMonth = c.get(Calendar.MONTH)
            val mDay = c.get(Calendar.DAY_OF_MONTH)

            instanceDialog = DatePickerDialog(context, style, object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(datePicker: DatePicker, year: Int, month: Int, day: Int) {
                    val date = GregorianCalendar(year, month, day).time
                    listener?.invoke(date)
                } }, mYear, mMonth, mDay)
                .apply {
                    setOnDismissListener { instanceDialog = null }
                    maxDate?.let { datePicker.maxDate = it }
                    minDate?.let { datePicker.minDate = it }

                    show()
                }
        }
    }

}