package ch.swissbytes.syscomappbase.views

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import ch.swissbytes.syscomappbase.R
import ch.swissbytes.syscomappbase.extensions.isInt

class NumberEditText : AppCompatEditText {

    private val TAG = this::class.java.simpleName

    var maxNum: Float = -1F

    var isCurrency: Boolean = false
    set(value) {
        field = value
        if (value)
            inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
        else
            inputType = InputType.TYPE_CLASS_NUMBER
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.NumberEditText, 0, 0)
        isCurrency = a.getBoolean(R.styleable.NumberEditText_is_currency, false)
        maxLines = 1

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            textDirection = TEXT_DIRECTION_ANY_RTL
            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
        }

        try {
            maxNum = a.getFloat(R.styleable.NumberEditText_max_input, -1f)
            this.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    setText("")
                } else {
                    text?.takeIf { it.isNotEmpty() }
                        ?.apply {
                            if (this[this.length - 1] == '.'){
                                setText(this.trim { it <= '.' })
                            } else if (this[0] == '.'){
                                setText("0$this")
                            }
                        }
                }
            }
            this.addTextChangedListener(textChangeWatcher)
        } finally {
            a.recycle()
        }
    }

    var textChangeWatcher = object : TextWatcher {

        var eraseFirstZero: Boolean = false
        var beforeText: String = "0"
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            eraseFirstZero = text.toString() == "0" && text?.length == 1
            beforeText = s.toString()
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            try {
                var value = s.toString()
                if (eraseFirstZero) {
                    value = value.replace("0$".toRegex(), "")
                    if (value.isEmpty()) {
                        value = "0"
                    }
                    s.replace(0, s.length, value, 0, value.length)
                } else {
                    if (value.contains('.') && value.substringAfterLast('.').length > 2){
                        s.replace(0, s.length, beforeText, 0, beforeText.length)
                    }
                }
                val parsedValue = java.lang.Float.parseFloat(value)
                if (maxNum != -1f /* mean it has not been set */ && parsedValue > maxNum) {
                    //Remove 100.00 <- trailing zero if the number is an int
                    val asString = if (maxNum.isInt()) maxNum.toInt().toString() else maxNum.toString()
                    s.replace(0, s.length, asString, 0, asString.length)
                }

                setSelection(s.length)
            } catch (ex: NumberFormatException) {

            }

        }
    }
}