package ch.swissbytes.syscomappbase.fragment


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import ch.swissbytes.syscomappbase.R

/**
 * @see  <a href="https://github.com/am5a03/viewstubfragment-demo/blob/master/app/src/main/java/com/raymond/viewstubfragmentdemo/HeavyFragment.kt">git demo</a>
 */
abstract class BaseViewStubFragment : Fragment() {
    private var mSavedInstanceState: Bundle? = null
    private var mHasInflated = false
    private var mViewStub: ViewStub? = null

    /**
     * The layout ID associated with this ViewStub
     * @see ViewStub.setLayoutResource
     * @return
     */
    protected abstract fun getViewStubLayoutResource(): Int
    protected abstract fun delayBeforeHideProgress(): Long?

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_view_stub, container, false).apply {
            mViewStub = findViewById(R.id.fragmentViewStub)
            mViewStub!!.layoutResource = getViewStubLayoutResource()
            mSavedInstanceState = savedInstanceState

            if (userVisibleHint && !mHasInflated) {
                val inflatedView = mViewStub!!.inflate()
                onCreateViewAfterViewStubInflated(inflatedView, mSavedInstanceState)
                afterViewStubInflated(this)
            }
        }


    protected abstract fun onCreateViewAfterViewStubInflated(view: View, savedInstanceState: Bundle?)

    /**
     *
     * @param originalViewContainerWithViewStub
     */
    @CallSuper
    protected fun afterViewStubInflated(originalViewContainerWithViewStub: View?) {
        mHasInflated = true
        val delay = delayBeforeHideProgress() ?: 0L
        Handler().postDelayed({
            originalViewContainerWithViewStub?.findViewById<View>(R.id.inflateProgressbar)?.apply { visibility = View.GONE }
        }, delay)

    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && mViewStub != null && !mHasInflated) {
            val inflatedView = mViewStub!!.inflate()
            onCreateViewAfterViewStubInflated(inflatedView, mSavedInstanceState)
            afterViewStubInflated(view)
        }
    }

    // Thanks to Noa Drach, this will fix the orientation change problem
    override fun onDetach() {
        super.onDetach()
        mHasInflated = false
    }
}