package ch.swissbytes.syscomappbase.fragment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


data class PagerFragment(var f: Fragment, var s: String)

class CPagerAdapter(fm: FragmentManager, pf: List<PagerFragment>) : FragmentPagerAdapter(fm) {

    private val mFragments = ArrayList<Fragment>()
    private val mFragmentTitles = ArrayList<String>()

    init {
        for (item in pf) {
            mFragments.add(item.f)
            mFragmentTitles.add(item.s)
        }
    }

    override fun getItem(position: Int): Fragment = mFragments[position]

    override fun getCount(): Int = mFragments.size

    override fun getPageTitle(position: Int): CharSequence? = mFragmentTitles[position]
}