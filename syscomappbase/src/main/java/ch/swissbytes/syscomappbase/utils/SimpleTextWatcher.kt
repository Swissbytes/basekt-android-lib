package ch.swissbytes.syscomappbase.utils

import android.text.Editable
import android.text.TextWatcher
import ch.swissbytes.syscomappbase.listeners.GenericListener

class SimpleTextWatcher(val listener: GenericListener<String>): TextWatcher {

    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        listener.invoke(s?.toString() ?: "")
    }
}