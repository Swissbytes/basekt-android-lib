package ch.swissbytes.syscomappbase.utils

import android.text.Editable
import android.text.TextWatcher
import java.math.BigDecimal

class NumberConstraintTextWatcher(
    private val minNum: BigDecimal? = null,
    private val maxNum: BigDecimal? = null,
    private val withDecimal: Boolean = false
): TextWatcher {

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        try {
            val input = s.toString()
            val value = BigDecimal(input)
            this.maxNum?.let {
                if (value.compareTo(maxNum) > 0) {
                    val asString = if (withDecimal) maxNum.toPlainString() else maxNum.toBigInteger().toString()
                    s.replace(0, s.length, asString, 0, asString.length)
                }
            }

            minNum?.let {
                if (value.compareTo(this.minNum) < 0) {
                    val asString = if (withDecimal) minNum.toPlainString() else minNum.toBigInteger().toString()
                    s.replace(0, s.length, asString, 0, asString.length)
                }
            }
        } catch (ex: Throwable) { //NumberFormatException

        }
    }
}
