package ch.swissbytes.syscomappbase.utils

import java.util.*

class CircularQueue<T>(val capacity: Int, default: T?): LinkedList<T>() {

    init { default?.let { for (i in 1..capacity) add(it) } }

    override fun add(element: T): Boolean {
        if(size >= capacity)
            removeFirst()
        return super.add(element);
    }
}