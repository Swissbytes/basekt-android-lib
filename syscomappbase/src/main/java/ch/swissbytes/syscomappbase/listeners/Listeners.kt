package ch.swissbytes.syscomappbase.listeners

typealias SimpleListener = () -> Unit
typealias GenericListener<T> = (T) -> Unit