package ch.swissbytes.syscomappbase.binders

import androidx.databinding.InverseMethod
import ch.swissbytes.syscomappbase.extensions.toFormatDecimalDefault
import ch.swissbytes.syscomappbase.extensions.zeroIfNull
import java.math.BigDecimal


object Convertor {

    @JvmStatic fun toBigDecimal(string: String): BigDecimal = string.replace(",", "").zeroIfNull()
    @JvmStatic fun toLiteralString(value: BigDecimal?): String = if (value == null) "0.00" else value.toFormatDecimalDefault()

    @JvmStatic @InverseMethod("toBigDecimal")
    fun toString(value: BigDecimal?): String = value?.toString() ?: "0.00"
}




