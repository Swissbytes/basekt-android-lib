package ch.swissbytes.syscomappbase.binders

import ch.swissbytes.syscomappbase.extensions.toFormatDecimalDefault
import java.math.BigDecimal


object CurrencyConvertor {

    @JvmStatic fun toLiteralBobUsdString(bob: BigDecimal?, usd: BigDecimal?): String {
        val boblit = if (bob == null) "0.00" else bob.toFormatDecimalDefault()
        val usdlit = if (usd == null) "0.00" else usd.toFormatDecimalDefault()
        return "$boblit Bs. ($usdlit Usd)"
    }

}
