package ch.swissbytes.syscomappbase.interfaces

import android.util.Log
import ch.swissbytes.syscomappbase.extensions.get
import ch.swissbytes.syscomappbase.listeners.GenericListener
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

abstract class SyscomBaseSync: SyscomSyncInterface {

    abstract val calls: Map<String, Observable<out Response<out Any>>>
    abstract val isDebug: Boolean
    abstract fun onResponse(model: Any)

    private val TAG = this::class.java.simpleName
    private var on_Sync_Fail: (GenericListener<Boolean>)? = null
    private var on_Progress: (GenericListener<String>)? = null
    private var appNeedUpdate: Boolean = false
    private var callsDone: Int = 0 ; private var errorOccure: Boolean = false

    override fun numOfCalls(): Int = calls.size

    override fun startSync(onProgress: GenericListener<String>, onSyncFail: GenericListener<Boolean>) {
        on_Progress = onProgress; on_Sync_Fail = onSyncFail
        for (call in calls){
            val callName = call.key
            val startTime = if (isDebug) System.currentTimeMillis() else 0
            call.value.subscribeOn(Schedulers.io())
                .get ({
                    if (isDebug) Log.w(TAG, "Calling method $callName: took ${System.currentTimeMillis() - startTime} mms")
                    val persistStart = if (isDebug) System.currentTimeMillis() else 0
                    onResponse(it!!)
                    if (isDebug) Log.w(TAG, "Persisting method $callName: took ${System.currentTimeMillis() - persistStart} mms")
                    checkAdvance(callName)
                }) {
                    Log.e("error $callName", it.toString())
                    if (it == 410) appNeedUpdate = true
                    errorOccure = true
                    checkAdvance(callName)
                }
        }
    }

    @Synchronized private fun checkAdvance(callName: String){
        if (calls.size == ++callsDone ){
            if (errorOccure){
                on_Sync_Fail?.invoke(appNeedUpdate)
            }
        }
        on_Progress?.invoke(callName)
    }

}