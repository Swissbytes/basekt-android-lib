package ch.swissbytes.syscomappbase.interfaces

import ch.swissbytes.syscomappbase.listeners.GenericListener

interface SyscomSyncInterface {
    fun numOfCalls(): Int
    fun startSync(onProgress: GenericListener<String>, onSyncFail: GenericListener<Boolean>)
}

interface Namable {
    var id: Long?
    var name: String?
    fun isLeaf(): Boolean = false
}
