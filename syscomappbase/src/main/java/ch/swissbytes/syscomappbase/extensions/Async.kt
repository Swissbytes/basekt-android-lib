package ch.swissbytes.syscomappbase.extensions

import android.os.AsyncTask

fun <T> doAsync(doAsync: () -> T) = Async(doAsync).execute()
fun <T> doAsync(doAsync: () -> T, then: ((T?) -> Unit)? = null) = Async(doAsync, then).execute()

class Async<T>(private val doAsync: () -> T, private val then: ((T?) -> Unit)? = null) : AsyncTask<Void, Void, T>() {

    override fun doInBackground(vararg params: Void?): T? = doAsync.invoke()

    override fun onPreExecute() {
        super.onPreExecute()
    }

    override fun onPostExecute(result: T?) {
        super.onPostExecute(result)
        then?.invoke(result)
    }
}