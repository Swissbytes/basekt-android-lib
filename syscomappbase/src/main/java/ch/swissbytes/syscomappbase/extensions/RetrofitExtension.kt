package ch.swissbytes.syscomappbase.extensions

import android.util.Log
import io.reactivex.Observable
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response



@SuppressWarnings("all")
fun <T> Observable<out Response<out T>>.get(onSuccess: (T?) -> Unit, onFail: (Int) -> Unit){
    subscribe({ r ->
        if (r.isSuccessful){
            onSuccess(r.body())
        } else {
            Log.e("Retrofit Error", r.errorBody().toString())
            onFail.invoke(r.code())
        }
    }, {
        Log.e("Retrofit Error", "UNKNOWN: ", it)
        NetworkError.apply { onFail.invoke(UNKNOWN) } })
}

@SuppressWarnings("all")
fun <T> Observable<out Response<out T>>.getOrThrowError(onSuccess: (T?) -> Unit, onFail: (String) -> Unit){
    subscribe({ r ->
        if (r.isSuccessful){
            onSuccess(r.body())
        } else {
            var errorMessage = r.errorBody()?.string() ?: ""

            r.errorBody()?.let { decodedError ->
                try {
                    errorMessage = JSONObject(decodedError.string()).getJSONObject("error").getString("message")
                } catch (e: JSONException){
                    Log.e("getOrThrowError cannot retrieve field error message", e.message)
                }
            }

            Log.e("Retrofit Error", errorMessage)
            onFail.invoke(errorMessage)
        }
    }, {
        Log.e("Retrofit Error", "UNKNOWN: ", it)
        NetworkError.apply { onFail.invoke(it.message ?: "UNKNOWN") } })
}