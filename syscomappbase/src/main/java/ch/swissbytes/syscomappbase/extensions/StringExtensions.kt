package ch.swissbytes.syscomappbase.extensions

import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

fun String?.zeroIfNull(): BigDecimal {
    return if (!this.isNumber()) BigDecimal.ZERO else BigDecimal(this)
}

fun String?.toFormatDecimalDefault(): String {
    val numberFormat = DecimalFormat("#,##0.00", DecimalFormatSymbols(Locale.US))
    return numberFormat.format(this?.toDouble() ?: 0.00)
}

fun String?.isNumber(): Boolean {
    if (this == null) return false
    try {
        java.lang.Double.parseDouble(this)
        return true
    } catch (e: NumberFormatException) {
        return false
    }
}

private fun String.onlyNumbers(): String = this.toCharArray()
    .filter { "0123456789".contains(it, true) }.joinToString("")
