package ch.swissbytes.syscomappbase.extensions
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import java.util.regex.Pattern


fun BigDecimal?.toFormattedString(): String {
    val value = this ?: BigDecimal.ZERO
    val symbols = DecimalFormatSymbols(Locale.US)
    val numberFormat = DecimalFormat("#,##0.00", symbols)
    return numberFormat.format(value.toDouble())
}

fun BigDecimal?.multiplyBy(value: BigDecimal): BigDecimal {
    val num = this ?: BigDecimal.ZERO
    return num.multiply(value).setScale(2, RoundingMode.UP)
}

fun String?.toFormattedString(): String {
    return this.toBigDecimalOrZero().toFormattedString()
}

fun Float?.isInt(): Boolean = if (this == null) false else compareTo(toInt()) == 0

fun Long?.toBigDecimalOrZero(): BigDecimal {
    val value = if (this == null) "0" else toString().replace(",", "")
    return BigDecimal(value)
}

fun String?.toBigDecimalOrZero(): BigDecimal {
    val value = if (isNullOrBlank()) "0" else this!!.replace(",", "")
    return BigDecimal(value)
}

infix fun BigDecimal.greaterOrEqualTo(amountB: BigDecimal?): Boolean {
    return amountB != null && (this.compareTo(amountB) >= 0)
}

infix fun BigDecimal.greaterThan(amountB: BigDecimal?): Boolean {
    return amountB != null && (this.compareTo(amountB) == 1)
}

fun BigDecimal.greaterThanZero(): Boolean {
    return this.compareTo(BigDecimal.ZERO) == 1
}

fun BigDecimal.lessOrEqualToZero(): Boolean {
    return !this.greaterThanZero()
}

fun BigDecimal.isZero(): Boolean {
    return this.compareTo(BigDecimal.ZERO) == 0
}

fun BigDecimal.toPositive(): BigDecimal {
    return if (!this.greaterThanZero()) this.negate() else this
}

fun BigDecimal.defaultScale(): BigDecimal = this.setScale(12, RoundingMode.HALF_UP)

infix fun BigDecimal.divideDefaultRounding(value: BigDecimal): BigDecimal = this.divide(value, 12, RoundingMode.HALF_UP)

infix fun BigDecimal.divide(value: BigDecimal): BigDecimal = this.defaultScale().divideDefaultRounding(value).defaultScale()

infix fun BigDecimal.divideNoRounding(value: BigDecimal): BigDecimal = this.divide(value, 10, RoundingMode.UP)

infix fun BigDecimal.divideOrZero(value: BigDecimal): BigDecimal {
    return if (this greaterOrEqualTo BigDecimal.ZERO) this divide value else BigDecimal.ZERO
}

fun Number?.toFormatDecimalDefault(): String {
    val numberFormat = DecimalFormat("#,##0.00", DecimalFormatSymbols(Locale.US))
    return numberFormat.format(this?.toDouble() ?: 0.00)
}


class NumberToLiteral {
    companion object {

        private val UNITS = arrayOf("", "un ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve ")
        private val DOZENS = arrayOf("diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ", "diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ", "cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa ")
        private val HUNDREDS = arrayOf("", "ciento ", "doscientos ", "trescientos ", "cuatrocientos ", "quinientos ", "seiscientos ", "setecientos ", "ochocientos ", "novecientos ")

        fun convert(number: String, uppercase: Boolean): String? {
            var number = number
            var literal = ""
            val part_decimal: String
            number = number.replace(".", ",")
            if (number.indexOf(",") == -1) {
                number = "$number,00"
            }
            if (Pattern.matches("\\d{1,9},\\d{1,3}", number)) {
                val Num = number.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                part_decimal = Num[1] + "/100 Bolivianos."
                if (Integer.parseInt(Num[0]) == 0) {
                    literal = "cero "
                } else if (Integer.parseInt(Num[0]) > 999999) {
                    literal = getMillions(Num[0])
                } else if (Integer.parseInt(Num[0]) > 999) {
                    literal = getMiles(Num[0])
                } else if (Integer.parseInt(Num[0]) > 99) {
                    literal = getHundreds(Num[0])
                } else if (Integer.parseInt(Num[0]) > 9) {
                    literal = getDozens(Num[0])
                } else {
                    literal = getUnits(Num[0])
                }
                return if (uppercase) {
                    (literal + part_decimal).toUpperCase()
                } else {
                    literal + part_decimal
                }
            } else {
                return null
            }
        }

        private fun getUnits(num: String): String {
            val numb = num.substring(num.length - 1)
            return UNITS[Integer.parseInt(numb)]
        }

        private fun getDozens(num: String): String {
            val n = Integer.parseInt(num)
            if (n < 10) {
                return getUnits(num)
            } else if (n > 19) {
                val u = getUnits(num)
                return if (u == "") {
                    DOZENS[Integer.parseInt(num.substring(0, 1)) + 8]
                } else {
                    DOZENS[Integer.parseInt(num.substring(0, 1)) + 8] + "y " + u
                }
            } else {
                return DOZENS[n - 10]
            }
        }

        private fun getHundreds(num: String): String {
            return if (Integer.parseInt(num) > 99) {
                if (Integer.parseInt(num) == 100) {
                    " cien "
                } else {
                    HUNDREDS[Integer.parseInt(num.substring(0, 1))] + getDozens(num.substring(1))
                }
            } else {
                getDozens(Integer.parseInt(num).toString() + "")
            }
        }

        private fun getMiles(number: String): String {
            val c = number.substring(number.length - 3)
            val m = number.substring(0, number.length - 3)
            if (Integer.parseInt(m) > 0) {
                val n = getHundreds(m)
                return n + "mil " + getHundreds(c)
            } else {
                return "" + getHundreds(c)
            }
        }

        private fun getMillions(number: String): String {
            val miles = number.substring(number.length - 6)
            val million = number.substring(0, number.length - 6)
            val n = if (million.length > 1) {
                getHundreds(million) + "millones "
            } else {
                getUnits(million) + "millon "
            }
            return n + getMiles(miles)
        }
    }
}

