package ch.swissbytes.syscomappbase.extensions

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Paint
import android.os.Build
import android.text.Html
import android.util.Base64
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import java.io.ByteArrayOutputStream

fun EditText.forceClearFocus(){
    isFocusableInTouchMode = false
    setFocusable(false)
    isFocusableInTouchMode = true
    setFocusable(true)
}

fun View.strikelineAllChilds(){
    when(this){ is TextView -> paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG }
    if (this is ViewGroup) {
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            child.strikelineAllChilds()
        }
    }
}

fun View.disableAllChilds(enabled: Boolean = false, opacity: Float = .5F){
    isEnabled = enabled
    isClickable = enabled
    alpha = if (enabled) 1F else opacity
    if (this is ViewGroup) {
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            child.disableAllChilds(enabled)
        }
    }
}

fun Bitmap.toBase64(): String{
    val byteArrayOutputStream = ByteArrayOutputStream().apply { compress(Bitmap.CompressFormat.PNG, 100, this) }
    return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
}

fun String.toBitmap(): Bitmap {
    val decodedString = Base64.decode(this, Base64.DEFAULT)
    return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
}

fun TextView.setHtmlText(s: String) {
    text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(s, Html.FROM_HTML_MODE_COMPACT) else  Html.fromHtml(s)
}