package ch.swissbytes.syscomappbase.extensions

fun <T> Iterable<T>.getUniqueOrNull(): T? = this.elementAtOrNull(0)