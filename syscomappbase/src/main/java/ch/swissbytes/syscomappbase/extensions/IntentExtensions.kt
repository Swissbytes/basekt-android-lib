package ch.swissbytes.syscomappbase.extensions

import android.content.Intent
import android.os.Parcelable


fun <T: Parcelable> Intent?.getHasMutableList(fieldName: String): MutableList<T> =
        if (this == null) mutableListOf() else ArrayList(this.getParcelableArrayListExtra(fieldName))