package ch.swissbytes.syscomappbase.extensions

import androidx.annotation.AnimRes
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import ch.swissbytes.syscomappbase.R


inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

inline fun FragmentManager.commitAllowingStateLoss(func: FragmentTransaction.() -> Unit) {
    val fragmentTransaction = beginTransaction()
    fragmentTransaction.func()
    fragmentTransaction.commitAllowingStateLoss()
}

fun AppCompatActivity.replaceFragmentAllowingStateLoss(fragment: Fragment, @IdRes frameId: Int, withFade: Boolean = true) {
    supportFragmentManager.commitAllowingStateLoss{
        if (withFade) { setCustomAnimations(R.anim.fade_in, R.anim.fade_out) }
        replace(frameId, fragment)}
}

fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int){
    supportFragmentManager.inTransaction { add(frameId, fragment) }
}

fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int, @AnimRes `in`: Int, @AnimRes `out`: Int){
    supportFragmentManager.inTransaction { setCustomAnimations(`in`, `out`); add(frameId, fragment) }
}


fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
    supportFragmentManager.inTransaction{ replace(frameId, fragment) }
}

fun AppCompatActivity.removeFragment(fragment: Fragment) {
    supportFragmentManager.inTransaction{ remove(fragment) }
}
