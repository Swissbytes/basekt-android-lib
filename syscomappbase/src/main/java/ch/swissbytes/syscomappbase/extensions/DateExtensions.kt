package ch.swissbytes.syscomappbase.extensions

import java.text.SimpleDateFormat
import java.util.*

fun Date?.toShortDateStringForInvoice(): String =
        if (this == null) "" else SimpleDateFormat("yyyyMMdd", Locale.US).format(this)


fun Date?.toShortDateString(): String =
        if (this == null) "" else this.time.fromLongToShortDateString()

fun Long?.fromLongToShortDateString(): String {
        val parser = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        parser.timeZone = TimeZone.getTimeZone("UTC")
        return if (this == null) "" else parser.format(Date(this))
}