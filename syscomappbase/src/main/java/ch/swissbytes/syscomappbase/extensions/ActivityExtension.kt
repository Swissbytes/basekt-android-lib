package ch.swissbytes.syscomappbase.extensions

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import ch.swissbytes.syscomappbase.listeners.SimpleListener


fun postDelayed(time: Long = 500L, listener: SimpleListener){
    Handler().postDelayed({ listener.invoke() }, time)
}

fun Context.getNavigationBarHeight(): Int {
    val id = resources.getIdentifier("navigation_bar_height", "dimen", "android").takeIf { it > 0 }
    return if (id != null) resources.getDimensionPixelSize(id) else 0
}


fun AppCompatActivity.screenWidth(): Int{
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun AppCompatActivity.screenHeight(): Int{
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}

fun Context.convertPixelsToDp(px: Float): Float =
    px / (resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)


fun AppCompatActivity.hideKeyboard() {
    hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Context.showToast(message: String, time: Int? = null){
    Handler(Looper.getMainLooper()).post {
        Toast.makeText(this, message, if (time == null) Toast.LENGTH_SHORT else time).show()
    }
}

fun Context.showToast(@StringRes message: Int, time: Int? = null){
    Handler(Looper.getMainLooper()).post {
        Toast.makeText(this, message, if (time == null) Toast.LENGTH_SHORT else time).show()
    }
}


