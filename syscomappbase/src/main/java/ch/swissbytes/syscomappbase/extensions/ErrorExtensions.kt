package ch.swissbytes.syscomappbase.extensions

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import android.widget.Toast
import ch.swissbytes.syscomappbase.R
import retrofit2.Response


class NetworkError{
    companion object {
        val NOT_FOUND: Int = 404
        val UNAUTHORIZED: Int = 401
        val UNKNOWN: Int = 500
    }
}

fun <T> Response<T>.unwrap(onSuccess: (T?) -> Unit, onFail: ((Int) -> Unit)? = null ) {
    if (this.isSuccessful){
        onSuccess(this.body())
    } else {
        onFail?.invoke(this.code())
    }
}

fun Int.showAsNetworkError(context: Context){
    context.showToast(when(this) {
        NetworkError.NOT_FOUND -> context.getString(R.string.network_404)
        NetworkError.UNAUTHORIZED -> context.getString(R.string.network_401)
        else -> context.getString(R.string.network_generic_error)
    })
}

fun Context.showToast(message: String){
   Handler(Looper.getMainLooper()).post {
       when(this){
           is Activity -> {
               try { if (!this.isFinishing) Toast.makeText(this, message, Toast.LENGTH_SHORT).show() }
               catch (e: WindowManager.BadTokenException) { }
           }
           else -> {
               try { Toast.makeText(this, message, Toast.LENGTH_SHORT).show() }
               catch (e: WindowManager.BadTokenException) { }

           }
       }
   }
}
